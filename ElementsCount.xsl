<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml" indent="yes"/>
    <xsl:key name="Elem" match="*|@*" use="name()"/>
    <xsl:template match="/">
        <Results>
            <xsl:apply-templates 
                select="(//*|//@*)[generate-id(.) = generate-id(key('Elem', name()))]"/>
        </Results>
    </xsl:template>
    
    <xsl:template match="*|@*">
        <xsl:value-of select="concat('&#xa;Node &#8242;', name(), '&#8242; found ', count(key('Elem', name())), ' times.')"/>
    </xsl:template>
    
</xsl:stylesheet>