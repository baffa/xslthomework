<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    

    <xsl:template match="/">
        <Guests>
            <xsl:apply-templates select="//*[local-name() = 'Guest']"/>
        </Guests>
    </xsl:template>

    <xsl:template match="//*[local-name() = 'Guest']">
        <xsl:value-of select="*[local-name() = 'Type']"/>|<xsl:value-of
            select="*[local-name() = 'Profile']/*[local-name() = 'Address']"/>|<xsl:value-of select="@Age"/>|<xsl:value-of
            select="@Nationalty"/>|<xsl:value-of select="@Gender"/>|<xsl:value-of select="@Name"/>#
    </xsl:template>
</xsl:stylesheet>
