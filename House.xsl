<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml" indent="yes"/>

    <xsl:variable name="houseRoomsCount" select="count(//*[local-name() = 'Room'])"/>
    <xsl:variable name="houseGuestsCount" select="sum(//@guests)"/>

    <xsl:template match="/">
        <AllRooms>
            <xsl:apply-templates select="//*[local-name(.) = 'House']">
                <xsl:sort select="@City"/>
                <xsl:sort select="./*[local-name() = 'Address']"/>
            </xsl:apply-templates>
        </AllRooms>
    </xsl:template>

    <xsl:template match="//*[local-name(.) = 'House']">
        <xsl:apply-templates select=".//*[local-name() = 'Block']">
            <xsl:sort data-type="number" select="@number"/>
            <xsl:with-param name="city" select="@City"/>
            <xsl:with-param name="address" select="*[local-name() = 'Address']"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="//*[local-name(.) = 'Block']">
        <xsl:param name="city"/>
        <xsl:param name="address"/>
        <xsl:variable name="rooms" select=".//*[local-name() = 'Room']"/>
        <xsl:apply-templates select="$rooms">
            <xsl:sort data-type="number" select="@nuber"/>
            <xsl:with-param name="city" select="$city"/>
            <xsl:with-param name="address" select="$address"/>
            <xsl:with-param name="blockName" select="@number"/>
            <xsl:with-param name="blockCount" select="sum($rooms/@guests)"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="//*[local-name(.) = 'Room']">
        <xsl:param name="city"/>
        <xsl:param name="address"/>
        <xsl:param name="blockCount"/>
        <xsl:param name="blockName"/>
        <Room>
            <Address><xsl:call-template name="uppercase">
                    <xsl:with-param name="str" select="$city"/>
                </xsl:call-template>/<xsl:value-of select="$address"/>/<xsl:value-of
                    select="$blockName"/>/<xsl:value-of select="@nuber"/></Address>
            <HouseRoomsCount>
                <xsl:value-of select="$houseRoomsCount"/>
            </HouseRoomsCount>
            <BlockRoomsCount>
                <xsl:value-of select="$blockCount"/>
            </BlockRoomsCount>
            <HouseGuestsCount>
                <xsl:value-of select="$houseGuestsCount"/>
            </HouseGuestsCount>
            <GuestsPerRoomAverage>
                <xsl:value-of select="floor($houseGuestsCount div $houseRoomsCount)"/>
            </GuestsPerRoomAverage>
            <xsl:choose>
                <xsl:when test="@guests = 1">
                    <Allocated Single="true" Double="false" Triple="false" Quarter="false"/>
                </xsl:when>
                <xsl:when test="@guests = 2">
                    <Allocated Single="false" Double="true" Triple="false" Quarter="false"/>
                </xsl:when>
                <xsl:when test="@guests = 3">
                    <Allocated Single="false" Double="false" Triple="true" Quarter="false"/>
                </xsl:when>
                <xsl:otherwise>
                    <Allocated Single="false" Double="false" Triple="false" Quarter="true"/>
                </xsl:otherwise>
            </xsl:choose>
        </Room>
    </xsl:template>

    <xsl:template name="uppercase">
        <xsl:param name="str"/>
        <xsl:variable name="upAlphabet" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'"/>
        <xsl:variable name="lowAlphabet" select="'abcdefghijklmnopqrstuvwxyz'"/>
        <xsl:value-of select="translate($str, $lowAlphabet, $upAlphabet)"/>
    </xsl:template>

</xsl:stylesheet>
