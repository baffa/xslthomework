<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml" indent="yes"/>

    <xsl:template match="/">
        <Guests>
            <xsl:apply-templates/>
        </Guests>
    </xsl:template>

    <xsl:template match="text()" name="guestTemplate">
        <xsl:param name="text" select="normalize-space(.)"/>
        <xsl:if test="contains($text, '#')">
            <xsl:call-template name="buildTemplate">
                <xsl:with-param name="info" select="substring-before($text, '#')"/>
            </xsl:call-template>
            <xsl:call-template name="guestTemplate">
                <xsl:with-param name="text" select="substring-after($text, '#')"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>

    <xsl:template name="buildTemplate">
        <xsl:param name="info"/>
        <xsl:variable name="afterType" select="substring-after($info,'|')"/>
        <xsl:variable name="afterAddress" select="substring-after($afterType, '|')"/>
        <xsl:variable name="afterAge" select="substring-after($afterAddress, '|')"/>
        <xsl:variable name="afterNationalty" select="substring-after($afterAge,'|')"/>
        <xsl:variable name="afterGender" select="substring-after($afterNationalty, '|')"/>
        <Guest Age="{substring-before($afterAddress, '|')}" Nationalty="{substring-before($afterAge, '|')}"
            Gender="{substring-before($afterNationalty, '|')}" Name="{$afterGender}">
            <Type><xsl:value-of select="substring-before($info, '|')"/></Type>
            <Profile>
                <Address><xsl:value-of select="substring-before($afterType, '|')"/></Address>
            </Profile>
        </Guest>
    </xsl:template>

</xsl:stylesheet>
