<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml" indent="yes"/>

    <xsl:variable select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" name="upper"/>
    <xsl:variable select="'abcdefghijklmnopqrstuvwxyz'" name="lower"/>

    <xsl:template match="/">
           <xsl:apply-templates select="node()"/>
    </xsl:template>

    <xsl:template match="*[contains(local-name(), 'Guest')]">
        <xsl:element name="{concat('People', substring(local-name(), 6))}" namespace="{namespace-uri()}">
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="*">
        <xsl:element name="{name()}" namespace="{namespace-uri()}">
            <xsl:for-each select="@*">
                <xsl:attribute name="{translate(name(), $lower, $upper)}">
                    <xsl:value-of select="."/>
                </xsl:attribute>
            </xsl:for-each>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="text()[normalize-space(.)]">
        <Text>
            <xsl:value-of select="."/>
        </Text>
    </xsl:template>

</xsl:stylesheet>
