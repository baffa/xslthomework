<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml" indent="yes"/>
    <xsl:key name="AlphabetKey" match="/*/*" use="substring(@*, 1, 1)"/>

    <xsl:template match="/">
        <list>
            <xsl:apply-templates
                select="/*/*[generate-id(.) = generate-id(key('AlphabetKey', substring(@*, 1, 1)))]">
                <xsl:sort select="substring(@*, 1, 1)"/>
            </xsl:apply-templates>
        </list>
    </xsl:template>


    <xsl:template match="*">
        <xsl:variable name="letter" select="substring(@*, 1, 1)"/>
        <capital value="{$letter}">
            <xsl:for-each select="/*/*">
                <xsl:if test="starts-with(@*, $letter)">
                    <name>
                        <xsl:value-of select="@*"/>
                    </name>
                </xsl:if>
            </xsl:for-each>
        </capital>
    </xsl:template>
</xsl:stylesheet>
